﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Gelf4Net.Appender;
using Gelf4Net.Layout;
using log4net;
using log4net.Repository.Hierarchy;

namespace GenericRedirector
{
    public class MvcApplication : HttpApplication
    {
        private const string Log4NetUrlCustomPropertyName = "url";
        private const string Log4NetHostCustomPropertyName = "host";
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Manage error on web site
        /// </summary>
        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            if (ex != null)
                log.Error("Unhandled exception error has occurred. Error:: " + ex.Message + ex.StackTrace,
                    ex);
        }

        private void ConfigureLog4Net()
        {
            var hierarchy = (Hierarchy) LogManager.GetRepository();
            foreach (var appender in hierarchy.Root.Appenders.OfType<AsyncGelfUdpAppender>())

            {
                var layout = appender.Layout as GelfLayout;
                if (layout != null)
                {
                    var siteName = HostingEnvironment.SiteName;
                    var assembly = Assembly.GetExecutingAssembly();
                    var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                    var version = fvi.FileVersion;
                    layout.AdditionalFields = string.Format(
                        "Host:%property{{{3}}},Version:{0},Level:%level,SiteName:{1},Url:%property{{{2}}},Method:%m",
                        version, siteName, Log4NetUrlCustomPropertyName, Log4NetHostCustomPropertyName);
                }

                appender.ActivateOptions();
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            LogicalThreadContext.Properties[Log4NetUrlCustomPropertyName] =
                HttpContext.Current.Request.Url.AbsoluteUri;
            LogicalThreadContext.Properties[Log4NetHostCustomPropertyName] =
                HttpContext.Current.Request.Url.Host;
        }

        protected void Application_Start()
        {
            ConfigureLog4Net();
//            log.WarnFormat("Starting site {0} on server {1}", HostingEnvironment.SiteName, Environment.MachineName);

            log.WarnFormat("Started site {0} on server {1}", HostingEnvironment.SiteName, Environment.MachineName);
        }

        protected void Application_Stop()
        {
            log.WarnFormat("Stopping site {0} on server {1}", HostingEnvironment.SiteName, Environment.MachineName);
        }
    }
}