﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace GenericRedirector
{
    public class ServiceLocator
    {
        private readonly WindsorContainer _container = new WindsorContainer();

        private ServiceLocator()
        {
            _container.Register(Component.For<IUrlChecker>().ImplementedBy<UrlChecker>());
        }

        public static ServiceLocator Current { get; } = new ServiceLocator();

        public T Get<T>()
        {
            return _container.Resolve<T>();
        }
    }
}