﻿namespace GenericRedirector
{
    public class RedirectInfo
    {
        public bool Success { get; set; }
        public string NewUrl { get; set; }
        public int StatusCode { get; set; }
    }
}