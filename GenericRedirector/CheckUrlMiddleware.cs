﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Hosting;
using log4net;
using Microsoft.Owin;

namespace GenericRedirector
{
    public class CheckUrlMiddleware : OwinMiddleware
    {
        private const string _cacheKey = "25ebd004-bff8-401c-8fd1-f03532df5f82";
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUrlChecker _checker;

        private readonly string _html404Path;

        public CheckUrlMiddleware(OwinMiddleware next, IUrlChecker checker, string html404Path) : base(next)
        {
            _checker = checker;
            _html404Path = html404Path;
        }

        private byte[] Get404Bytes()
        {
            return GetCachedBytes(_html404Path);
        }

        private byte[] GetFaviconBytes()
        {
            return GetCachedBytes(HostingEnvironment.MapPath("~/app_data/favicon.ico"));
        }

        private static byte[] GetCachedBytes(string path)
        {
            var bytes = MemoryCache.Default.Get(path) as byte[];
            if (bytes == null)
            {
                bytes = File.ReadAllBytes(path);
                MemoryCache.Default.Add(path, bytes,
                    new CacheItemPolicy
                    {
                        AbsoluteExpiration = DateTimeOffset.MaxValue,
                        ChangeMonitors = {new HostFileChangeMonitor(new List<string> {path})}
                    });
            }

            return bytes;
        }

        public override async Task Invoke(IOwinContext context)
        {
            var correlationId = Guid.NewGuid();
            if (log.IsDebugEnabled)
            {
                string ip = context.Request.RemoteIpAddress;
                if (context.Request.Headers.ContainsKey("x-forwarded-for"))
                {
                    ip = context.Request.Headers["x-forwarded-for"];
                }
                string referrer = null;
                if (context.Request.Headers.ContainsKey("referer")) referrer = context.Request.Headers["referer"];
                log.DebugFormat("Handling request for {0}, CID = {1}, from client {2} with referrer '{3}'",
                    context.Request.Uri, correlationId,
                    ip, referrer);
            }

            var url = context.Request.Uri;
            //looking for favicon?
            if (url.AbsolutePath.Equals("/favicon.ico", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.Write(GetFaviconBytes());
                context.Response.StatusCode = 200;
                context.Response.ContentType = "image/ico";
                return;
            }

            if (!url.AbsolutePath.Equals("/404"))
            {
                var redirectInfo = _checker.GetRedirectInfo(url, correlationId);
                if (redirectInfo.Success)
                {
                    context.Response.StatusCode = redirectInfo.StatusCode;
                    context.Response.Headers.Set("Location", redirectInfo.NewUrl);
                    return;
                }
            }


            //404
            log.DebugFormat("Returning 404 page, CID = {0}", correlationId);
            await context.Response.WriteAsync(Get404Bytes());
            context.Response.ContentType = "text/html";
            context.Response.StatusCode = 404;
        }
    }
}