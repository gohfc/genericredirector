﻿using System;

namespace GenericRedirector
{
    public interface IUrlChecker
    {
        RedirectInfo GetRedirectInfo(Uri uri, Guid correlationId);
    }
}