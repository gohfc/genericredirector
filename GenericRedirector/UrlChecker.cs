﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using GenericRedirector.Entities;
using log4net;

namespace GenericRedirector
{
    public class UrlChecker : IUrlChecker
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly SqlConnectionStringBuilder _sqlConnectionStringBuilder;

        public UrlChecker()
        {
            using (var ctx = new RedirectorEntities())
            {
                _sqlConnectionStringBuilder = new SqlConnectionStringBuilder(ctx.Database.Connection.ConnectionString);
            }
        }

        public RedirectInfo GetRedirectInfo(Uri uri, Guid correlationId)
        {
            log.DebugFormat("Processing CID {0} using database {1} on server {2} as user {3}", correlationId,
                _sqlConnectionStringBuilder.InitialCatalog, _sqlConnectionStringBuilder.DataSource,
                _sqlConnectionStringBuilder.UserID);
            try
            {
                Rule rule;
                using (var context = new RedirectorEntities())
                {
                    var urlHost = uri.Host;
                    var hostAndPort = string.Format("{0}:{1}", urlHost, uri.Port);
                    log.DebugFormat("Looking for a site with a binding to match '{0}' or '{1}'", urlHost, hostAndPort);
                    var sites = context.Sites.Where(
                        i => i.Bindings.Any(j => j.Name == urlHost || j.Name == hostAndPort));

                    rule = null;
                    foreach (var site in sites)
                    {
                        foreach (var siteRule in site.Rules)
                        {
                            if (siteRule.SourcePath.ToLowerInvariant().TrimEnd('/') == uri.AbsolutePath.ToLowerInvariant().TrimEnd('/'))
                            {
                                rule = siteRule;
                                break;
                            }
                        }
                    }

                    if (rule != null)
                    {
                        log.DebugFormat("Exact match found for path.  Redirecting to {0}.  CID {1}",
                            rule.DestinationUrl, correlationId);
                        return new RedirectInfo {NewUrl = rule.DestinationUrl, StatusCode = 301, Success = true};
                    }
                    else
                    {
                        if (sites.Any())
                            log.DebugFormat("Have binding, but no rule found for url {0}, CID {1}", uri, correlationId);
                        else
                            log.DebugFormat("No binding, no rule found for url {0}, CID {1}", uri, correlationId);
                    }

                    return new RedirectInfo {Success = false};
                }
            }
            finally
            {
                log.DebugFormat("Done with CID {0}", correlationId);
            }
        }
    }
}