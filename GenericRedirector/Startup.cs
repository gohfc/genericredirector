﻿using System.Web.Hosting;
using GenericRedirector;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace GenericRedirector
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var urlChecker = ServiceLocator.Current.Get<IUrlChecker>();
            app.Use<CheckUrlMiddleware>(urlChecker, HostingEnvironment.MapPath("~/app_data/404.html"));
        }
    }
}