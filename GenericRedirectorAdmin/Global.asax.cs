﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Gelf4Net.Appender;
using Gelf4Net.Layout;
using log4net;
using log4net.Repository.Hierarchy;

namespace GenericRedirectorAdmin
{
    public class Global : HttpApplication
    {
       
        private const string Log4NetUrlCustomPropertyName = "url";
        private const string Log4NetHostCustomPropertyName = "host";
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Manage error on web site
        /// </summary>
        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            if (ex != null)
                log.Error("Unhandled exception error has occurred. Error:: " + ex.Message + ex.StackTrace,
                    ex);
        }

        private void ConfigureLog4Net()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();
            foreach (var appender in hierarchy.Root.Appenders.OfType<AsyncGelfUdpAppender>())

            {
                var layout = appender.Layout as GelfLayout;
                if (layout != null)
                {
                    var siteName = HostingEnvironment.SiteName;
                    var assembly = Assembly.GetExecutingAssembly();
                    var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                    var version = fvi.FileVersion;
                    layout.AdditionalFields = string.Format(
                        "Host:%property{{{3}}},Version:{0},Level:%level,SiteName:{1},Url:%property{{{2}}}",
                        version, siteName, Log4NetUrlCustomPropertyName, Log4NetHostCustomPropertyName);
                }

                appender.ActivateOptions();
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            LogicalThreadContext.Properties[Log4NetUrlCustomPropertyName] =
                HttpContext.Current.Request.Url.AbsoluteUri;
            LogicalThreadContext.Properties[Log4NetHostCustomPropertyName] =
                HttpContext.Current.Request.Url.Host;
        }

        protected void Application_Start()
        {
            //Enable lazy loading, deferred JS, tokenization for all html pages


            ConfigureLog4Net();
            log.WarnFormat("Starting site {0} on server {1}", HostingEnvironment.SiteName, Environment.MachineName);

            //// Code that runs on application startup
            //AreaRegistration.RegisterAllAreas();

            //RouteConfig.RegisterRoutes(RouteTable.Routes);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //UnityConfig.RegisterTypes();
            //log.Error("this is a tets for vendor escalation!");


            log.WarnFormat("Started site {0} on server {1}", HostingEnvironment.SiteName, Environment.MachineName);
        }
    }
}