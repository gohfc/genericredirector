﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericRedirectorAdmin.Code
{
    public class Constants
    {
        public const string AuthorizedRoles = "BBI_CORP\\Administrators,BBI_CORP\\WebAdmins,BBI_CORP\\WebEditors";
    }
}