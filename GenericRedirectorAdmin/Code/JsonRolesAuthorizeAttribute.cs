﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace GenericRedirectorAdmin.Code
{
    /// <summary>
    ///     Specifies that access to a controller or action method is restricted to users who meet the authorization
    ///     requirement.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class JsonRolesAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
      

        private string[] Roles
        {
            get
            {
             const string key = "e34a4091-a355-461e-bbbf-0fc8b77f2175";
                var items = MemoryCache.Default[key] as string[];
                if (items == null)
                {
                    var jsonFileName = HostingEnvironment.MapPath("~/app_data/authorizedRoles.json");
                    var jsonText = System.IO.File.ReadAllText(jsonFileName);
                    items = JsonConvert.DeserializeObject<string[]>(
                        jsonText);
                    MemoryCache.Default.Add(key, items,
                        new CacheItemPolicy
                        {
                            AbsoluteExpiration = DateTimeOffset.MaxValue,
                            ChangeMonitors = {new HostFileChangeMonitor(new List<string> {jsonFileName})}
                        });
                }

                return items;
            }
        }


        /// <summary>Gets the unique identifier for this attribute.</summary>
        /// <returns>The unique identifier for this attribute.</returns>
        public override object TypeId { get; } = new object();

        /// <summary>Called when a process requests authorization.</summary>
        /// <param name="filterContext">
        ///     The filter context, which encapsulates information for using
        ///     <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="filterContext" /> parameter is null.</exception>
        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException(nameof(filterContext));
            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
                throw new InvalidOperationException("Can't use this within a child action");
            if ((filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ? 1 :
                    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute),
                        true) ? 1 : 0) != 0)
                return;
            if (AuthorizeCore(filterContext.HttpContext))
            {
                var cache = filterContext.HttpContext.Response.Cache;
                cache.SetProxyMaxAge(new TimeSpan(0L));
                cache.AddValidationCallback(CacheValidateHandler, null);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }


        /// <summary>When overridden, provides an entry point for custom authorization checks.</summary>
        /// <returns>true if the user is authorized; otherwise, false.</returns>
        /// <param name="httpContext">
        ///     The HTTP context, which encapsulates all HTTP-specific information about an individual HTTP
        ///     request.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="httpContext" /> parameter is null.</exception>
        protected virtual bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));
            var user = httpContext.User;
            return user.Identity.IsAuthenticated && (Roles.Length == 0 || Roles.Any(user.IsInRole));
        }

        private void CacheValidateHandler(
            HttpContext context,
            object data,
            ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        /// <summary>Processes HTTP requests that fail authorization.</summary>
        /// <param name="filterContext">
        ///     Encapsulates the information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.
        ///     The <paramref name="filterContext" /> object contains the controller, HTTP context, request context, action result,
        ///     and route data.
        /// </param>
        protected virtual void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }

        /// <summary>Called when the caching module requests authorization.</summary>
        /// <returns>A reference to the validation status.</returns>
        /// <param name="httpContext">
        ///     The HTTP context, which encapsulates all HTTP-specific information about an individual HTTP
        ///     request.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="httpContext" /> parameter is null.</exception>
        protected virtual HttpValidationStatus OnCacheAuthorization(
            HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));
            return !AuthorizeCore(httpContext) ? HttpValidationStatus.IgnoreThisRequest : HttpValidationStatus.Valid;
        }
    }
}