﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using GenericRedirector.Entities;
using GenericRedirectorAdmin.Code;
using Newtonsoft.Json;

namespace GenericRedirectorAdmin.Controllers
{
    [JsonRolesAuthorize]
    public class SiteController : EntityControllerBase<Site>
    {
        // GET: api/Site
        public IHttpActionResult Get([FromUri] ApiRequest request)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                var queryable = context.Sites.AsQueryable();
                if (!string.IsNullOrWhiteSpace(request._Filters))
                {
                    var l = JsonConvert.DeserializeObject<Dictionary<string, object>>(request._Filters);
                    foreach (var s in l)
                        switch (s.Key)
                        {
                            case "q":
                                var query = s.Value as string;
                                if (!string.IsNullOrEmpty(query))
                                {
                                    queryable = queryable.Where(i =>
                                        i.Name.Contains(query));
                                }

                                break;
                            case nameof(Site.SiteId):
                                var x = Convert.ToInt32(s.Value);
                                queryable = queryable.Where(i => i.SiteId == x);
                                break;
                            default:
                                Debug.Assert(true);
                                break;
                        }
                }


                return Getter(request, queryable, i => i.SiteId);
            }
        }


        // GET: api/Site/5
        public Site Get(int id)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                return context.Sites.FirstOrDefault(i => i.SiteId == id);
            }
        }

        // POST: api/Site
        public Site Post([FromBody] Site value)
        {
            using (var context = new RedirectorEntities())
            {
                value.CreatedUtc = DateTime.UtcNow;
                value.UpdatedUtc = DateTime.UtcNow;
                context.Sites.Add(value);
                context.SaveChanges();
                return value;
            }
        }

        // PUT: api/Site/5
        public IHttpActionResult Put([FromBody] Site value)
        {
            using (var context = new RedirectorEntities())
            {
                var existing = context.Sites.FirstOrDefault(i => i.SiteId == value.SiteId);
                if (existing != null)
                {
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Name = value.Name;
                    context.SaveChanges();
                    return Ok();
                }

                return NotFound();
            }
        }

        // DELETE: api/Site/5
        public void Delete(int id)
        {
            using (var context = new RedirectorEntities())
            {
                var site = new Site {SiteId = id};
                context.Entry(site).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        protected override void Unproxify(Site item)
        {
            item.Bindings = null;
            item.Rules = null;
        }
    }
}