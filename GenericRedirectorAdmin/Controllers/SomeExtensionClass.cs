﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace GenericRedirectorAdmin.Controllers
{
    public static class SomeExtensionClass
    {
        public static IQueryable<T> OrderByField<T>(this IQueryable<T> queryable, string sortField, bool ascending)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var prop = Expression.Property(param, sortField);
            var exp = Expression.Lambda(prop, param);
            var method = ascending ? nameof(Queryable.OrderBy) : nameof(Queryable.OrderByDescending);
            Type[] types = {queryable.ElementType, exp.Body.Type};
            var methodCallExpression = Expression.Call(typeof(Queryable), method, types, queryable.Expression, exp);
            return queryable.Provider.CreateQuery<T>(methodCallExpression);
        }
    }
}