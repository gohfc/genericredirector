﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using GenericRedirector.Entities;
using GenericRedirectorAdmin.Code;
using Newtonsoft.Json;

namespace GenericRedirectorAdmin.Controllers
{
    [JsonRolesAuthorize]
    public class BindingController : EntityControllerBase<Binding>
    {
        // GET: api/Site
        public IHttpActionResult Get([FromUri] ApiRequest request)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                var queryable = context.Bindings.AsQueryable();
                if (!string.IsNullOrWhiteSpace(request._Filters))
                {
                    var l = JsonConvert.DeserializeObject<Dictionary<string, object>>(request._Filters);
                    foreach (var s in l)
                        switch (s.Key)
                        {
                            case nameof(Binding.SiteId):
                                var x = Convert.ToInt32(s.Value);
                                queryable = queryable.Where(i => i.SiteId == x);
                                break;
                            default:
                                Debug.Assert(true);
                                break;
                        }
                }

                return Getter(request, queryable, i => i.BindingId);
            }
        }

        // GET: api/Site/5
        public Binding Get(int id)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                return context.Bindings.FirstOrDefault(i => i.BindingId == id);
            }
        }

        // POST: api/Site
        public Binding Post([FromBody] Binding value)
        {
            using (var context = new RedirectorEntities())
            {
                value.UpdatedUtc = DateTime.UtcNow;
                value.CreatedUtc = DateTime.UtcNow;

                context.Bindings.Add(value);
                context.SaveChanges();
                return value;
            }
        }

        // PUT: api/Site/5
        public IHttpActionResult Put([FromBody] Binding value)
        {
            using (var context = new RedirectorEntities())
            {
                var existing = context.Bindings.FirstOrDefault(i => i.BindingId == value.BindingId);
                if (existing != null)
                {
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Name = value.Name;
                    context.SaveChanges();
                    return Ok();
                }

                return NotFound();
            }
        }

        // DELETE: api/Site/5
        public void Delete(int id)
        {
            using (var context = new RedirectorEntities())
            {
                context.Entry(new Binding {BindingId = id}).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        protected override void Unproxify(Binding item)
        {
            item.Site = null;
        }
    }
}