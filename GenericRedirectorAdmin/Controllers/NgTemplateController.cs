﻿using System.Web.Mvc;
using GenericRedirectorAdmin.Code;

namespace GenericRedirectorAdmin.Controllers
{
    [System.Web.Http.Authorize(Users = Constants.AuthorizedRoles)]
    public class NgTemplateController : Controller
    {
        [JsonRolesAuthorize]
        // GET: Home
        public ActionResult Import()
        {
            return View();
        }
    }
}