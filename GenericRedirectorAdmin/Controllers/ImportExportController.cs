﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExcelDataReader;
using GenericRedirector.Entities;

namespace GenericRedirectorAdmin.Controllers
{
    public class ImportExportController : Controller
    {
        [HttpGet]
        public ActionResult Export(int siteId)
        {
            using (var context = new RedirectorEntities())
            {
                var site = context.Sites.FirstOrDefault(i => i.SiteId == siteId);
                if (site == null) return HttpNotFound();
                var rules = context.Rules.Where(i => i.SiteId == siteId).OrderBy(i => i.SourcePath);
                var memoryStream = new MemoryStream();
                var streamWriter = new StreamWriter(memoryStream);
                streamWriter.WriteLine("SourcePath,DestinationUrl");
                foreach (var rule in rules)
                    streamWriter.WriteLine("\"{0}\",\"{1}\"", rule.SourcePath, rule.DestinationUrl);
                streamWriter.Flush();
                memoryStream.Position = 0;
                return File(memoryStream, "text/plain", string.Format("{0}-rules.csv", site.Name));
            }
        }

        [HttpPost]
        public ActionResult DoImport(HttpPostedFileBase file, int siteId)
        {
            using (var context = new RedirectorEntities())
            {
                var statusInfo = new ImportStatusInfo();
                try
                {
                    using (var reader = GetReaderByExtension(file))
                    {
                        var columnInfo = new ColumnInfo();

                        while (reader.Read())
                        {
                            if (statusInfo.CurrentLine == 1)
                                ReadColumnInfo(reader, columnInfo);
                            else
                                ReadRow(siteId, reader, columnInfo, statusInfo, context);

                            statusInfo.CurrentLine++;
                        }

                        return Json(new
                        {
                            completed = true, errors = statusInfo.Errors,
                            message = string.Format("{0} rules were inserted or updated, and {1} errors occurred.",
                                statusInfo.SuccessCount, statusInfo.Errors.Count)
                        });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new {completed = false, message = ex.Message});
                }
            }
        }

        /// <summary>
        ///     read a non-header row from the input file
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="reader"></param>
        /// <param name="columnInfo"></param>
        /// <param name="statusInfo"></param>
        /// <param name="context"></param>
        private static void ReadRow(int siteId, IExcelDataReader reader, ColumnInfo columnInfo,
            ImportStatusInfo statusInfo,
            RedirectorEntities context)
        {
            var sourcePath = reader.GetString(columnInfo.sourcePathColumn.Value);
            var destinationUrl = reader.GetString(columnInfo.destinationUrlColumn.Value);
            if (string.IsNullOrWhiteSpace(sourcePath))
            {
                statusInfo.AddError("Source path can't be blank");
            }
            else if (string.IsNullOrWhiteSpace(destinationUrl))
            {
                statusInfo.AddError("Destination url can't be blank");
            }
            else if (!sourcePath.StartsWith("/"))
            {
                statusInfo.AddError("Source path must start with '/'");
            }
            else
            {
                Uri tmpUrl;
                if (!Uri.TryCreate(destinationUrl, UriKind.Absolute, out tmpUrl))
                {
                    statusInfo.AddError(string.Format("Destination url '{0}' is invalid",
                        destinationUrl));
                }
                else
                {
                    var existing = context.Rules.FirstOrDefault(i =>
                        i.SiteId == siteId && i.SourcePath == sourcePath);
                    if (existing != null)
                    {
                        existing.DestinationUrl = destinationUrl;
                        existing.UpdatedUtc = DateTime.UtcNow;
                        ;
                    }
                    else
                    {
                        context.Rules.Add(new Rule
                        {
                            DestinationUrl = tmpUrl.ToString(), SiteId = siteId,
                            SourcePath = sourcePath, CreatedUtc = DateTime.UtcNow, UpdatedUtc = DateTime.UtcNow
                        });
                    }

                    try
                    {
                        context.SaveChanges();
                        statusInfo.SuccessCount++;
                    }
                    catch (Exception ex)
                    {
                        statusInfo.AddError(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        ///     find out which column contains which data
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="columnInfo"></param>
        private static void ReadColumnInfo(IExcelDataReader reader, ColumnInfo columnInfo)
        {
            for (var i = 0; i < reader.FieldCount; i++)
            {
                var text = (reader.GetString(i) ?? "").ToLower();
                switch (text)
                {
                    case "sourcepath":
                        columnInfo.sourcePathColumn = i;
                        break;
                    case "destinationurl":
                        columnInfo.destinationUrlColumn = i;
                        break;
                }
            }

            if (columnInfo.sourcePathColumn == null && columnInfo.destinationUrlColumn == null)
                throw new FormatException("Can't find sourcePath or destinationUrl headers");
        }

        private static IExcelDataReader GetReaderByExtension(HttpPostedFileBase file)
        {
            var name = file?.FileName;
            if (!string.IsNullOrWhiteSpace(name))
            {
                var extension = Path.GetExtension(name).ToLower();
                switch (extension)
                {
                    case ".xls":
                    case ".xlsx":
                        return ExcelReaderFactory.CreateReader(file.InputStream);

                    case ".csv":
                        return ExcelReaderFactory.CreateCsvReader(file.InputStream, new ExcelReaderConfiguration
                        {
                            // Gets or sets the encoding to use when the input XLS lacks a CodePage
                            // record, or when the input CSV lacks a BOM and does not parse as UTF8. 
                            // Default: cp1252 (XLS BIFF2-5 and CSV only)
                            FallbackEncoding = Encoding.GetEncoding(1252)
                        });
                    default:
                        throw new FormatException(string.Format("Not sure what to do with file extension '{0}'",
                            extension));
                }
            }

            throw new FormatException("File name is blank.");
        }

        public class ColumnInfo
        {
            public int? sourcePathColumn { get; set; }
            public int? destinationUrlColumn { get; set; }
        }

        private class ImportStatusInfo
        {
            public int SuccessCount { get; set; }
            public int CurrentLine { get; set; } = 1;

            public List<string> Errors { get; } = new List<string>();

            public void AddError(string message)
            {
                Errors.Add(string.Format("Line {0} - {1}", CurrentLine, message));
            }
        }
    }
}