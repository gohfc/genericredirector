﻿namespace GenericRedirectorAdmin.Controllers
{
    public class ApiRequest
    {
        public int _Page { get; set; }
        public int _PerPage { get; set; }
        public string _SortDir { get; set; }
        public string _SortField { get; set; }
        public string _Filters { get; set; }
    }
}