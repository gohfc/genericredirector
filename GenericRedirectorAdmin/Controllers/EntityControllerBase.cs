﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;

namespace GenericRedirectorAdmin.Controllers
{
    public abstract class EntityControllerBase<T> : ApiController where T : class
    {
        protected abstract void Unproxify(T item);

        protected IHttpActionResult Getter(ApiRequest request, IQueryable<T> queryable,
            Expression<Func<T, int>> defaultSort)
        {
            if (!string.IsNullOrWhiteSpace(request._SortDir) &&
                !string.IsNullOrWhiteSpace(request._SortField) && request._SortField != "id")
                queryable = queryable.OrderByField(request._SortField, request._SortDir.ToLower() != "desc");
            else
                queryable = queryable.OrderBy(defaultSort);
            var count = queryable.Count();
            HttpContext.Current.Response.Headers["X-Total-Count"] = count.ToString();
            if (count == 0) return Ok(new T[] { });

            var start = (request._Page - 1) * request._PerPage;
            var list = queryable.Skip(start).Take(request._PerPage).ToList();
            foreach (var item in list) Unproxify(item);
            const int offset = 0;
            HttpContext.Current.Response.Headers["Content-Range"] =
                string.Format("entities {0}-{1}/{2}", start+offset, start + list.Count - 1+offset, count);

            return Ok(list);
        }
    }
}