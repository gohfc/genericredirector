﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using GenericRedirector.Entities;
using GenericRedirectorAdmin.Code;
using Newtonsoft.Json;

namespace GenericRedirectorAdmin.Controllers
{
    [JsonRolesAuthorize]
    public class RuleController : EntityControllerBase<Rule>
    {
        // GET: api/Site
        public IHttpActionResult Get([FromUri] ApiRequest request)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                var queryable = context.Rules.AsQueryable();
                if (!string.IsNullOrWhiteSpace(request._Filters))
                {
                    var deserializeObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(request._Filters);
                    foreach (var keyValuePair in deserializeObject)
                        switch (keyValuePair.Key)
                        {
                            case nameof(Rule.SiteId):
                                var siteId = Convert.ToInt32(keyValuePair.Value);
                                queryable = queryable.Where(i => i.SiteId == siteId);
                                break;
                            case "q":
                                var query = keyValuePair.Value as string;
                                if (!string.IsNullOrEmpty(query))
                                {
                                    queryable = queryable.Where(i =>
                                        i.DestinationUrl.Contains(query) || i.SourcePath.Contains(query));
                                }

                                break;

                            default:
                                Debug.Assert(true);
                                break;
                        }
                }

                return Getter(request, queryable, i => i.RuleId);
            }
        }

        // GET: api/Site/5
        public Rule Get(int id)
        {
            using (var context = new RedirectorEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                return context.Rules.FirstOrDefault(i => i.RuleId == id);
            }
        }

        // POST: api/Site
        public Rule Post([FromBody] Rule value)
        {
            using (var context = new RedirectorEntities())
            {
                value.CreatedUtc = DateTime.UtcNow;
                value.UpdatedUtc = DateTime.UtcNow;
                context.Rules.Add(value);
                context.SaveChanges();
                return value;
            }
        }

        // PUT: api/Site/5
        public IHttpActionResult Put([FromBody] Rule value)
        {
            using (var context = new RedirectorEntities())
            {
                var existing = context.Rules.FirstOrDefault(i => i.RuleId == value.RuleId);
                if (existing != null)
                {
                    existing.DestinationUrl = value.DestinationUrl;
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.SourcePath = value.SourcePath;
                    context.SaveChanges();
                    return Ok();
                }

                return NotFound();
            }
        }

        // DELETE: api/Site/5
        public void Delete(int id)
        {
            using (var context = new RedirectorEntities())
            {
                context.Entry(new Rule {RuleId = id}).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        protected override void Unproxify(Rule item)
        {
            item.Site = null;
        }
    }
}