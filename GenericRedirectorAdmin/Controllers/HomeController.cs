﻿using System.Web.Mvc;
using GenericRedirectorAdmin.Code;

namespace GenericRedirectorAdmin.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        [JsonRolesAuthorize]
        public ActionResult Index()
        {
            return RedirectToAction("Admin");
        }

        [JsonRolesAuthorize]
        public ActionResult Admin()
        {
            return View();
        }
    }
}