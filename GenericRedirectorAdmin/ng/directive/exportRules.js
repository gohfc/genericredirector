﻿myApp.directive('exportRules', [ function () {
    return {
        restrict: 'E',
        scope: { site: '&' },
        link: function (scope) {
            scope.getExportUrl = function () {
                return "/importexport/export?siteId=" + scope.site().values.SiteId;
            };
        },
        template: '<a class="btn btn-default btn-xs" ng-href="{{getExportUrl()}}">Export Rules</a>'
    };
}]);

myApp.directive('deleteRules', [function () {
    return {
        restrict: 'E',
        scope: { entries: '&' },
        link: function (scope) {
            debugger;
            
        },
        template: '<a class="btn btn-default btn-xs" href="abcd">Delete Rules</a>'
    };
}]);

 