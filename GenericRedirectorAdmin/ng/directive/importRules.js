﻿myApp.directive('importRules', ['$location', function ($location) {
    return {
        restrict: 'E',
        scope: { site: '&' },
        link: function (scope) {
            scope.importRules = function () {
                $location.path('/importRules/' + scope.site().values.SiteId);
            };
        },
        template: '<a class="btn btn-default btn-xs" ng-click="importRules()">Import Rules</a>'
    };
}]);

 