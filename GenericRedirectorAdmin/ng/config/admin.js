myApp.config([
    "NgAdminConfigurationProvider", "$stateProvider", function(nga, $stateProvider) {


        $stateProvider.state("import-rules",
            {
                parent: "ng-admin",
                url: "/importRules/:SiteId",
                params: { id: null },
                controller: "importRulesController",
                templateUrl: "/ngtemplate/import"
            });


        var entities = {
            site: null,
            binding: null,
            rule: null

        };

        var models = {
            site: {
                nameProperty: "Name",
                siteIdProperty: "SiteId",
                createdUtcProperty: "CreatedUtc",
                updatedUtcProperty: "UpdatedUtc"
            },
            rule: {
                sourcePathProperty: "SourcePath",
                destinationUrlProperty: "DestinationUrl",
                createdUtcProperty: "CreatedUtc",
                updatedUtcProperty: "UpdatedUtc"
            },
            binding: {
                nameProperty: "Name",
                createdUtcProperty: "CreatedUtc",
                updatedUtcProperty: "UpdatedUtc"
            }
        };


        const bindingItemName = "Binding";
        var bindingPluralLabel = bindingItemName + "s";
        var siteItemName = "Site";
        const siteGroupName = "Site";
        const bindingNameLabel = "Binding";
        var sitePluralLabel = siteItemName + "s";
        const ruleItemName = "Rule";
        var rulePluralLabel = ruleItemName + "s";

        let createdUtcLabel = "Created (UTC)";
        let updatedUtcLabel = "Updated (UTC)";
        let hiddenFieldClasses = "hidden-xs hidden-sm";
        var urlValidatorRegExp = new RegExp(
            /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i);

        var fields = {
            site: {
                name: nga.field(models.site.nameProperty).label(siteGroupName)
                    .validation({ required: true, maxlength: 50 }).isDetailLink(true),
                createdUtc: nga.field(models.site.createdUtcProperty, "datetime").label(createdUtcLabel)
                    .editable(false).cssClasses(hiddenFieldClasses),
                updatedUtc: nga.field(models.site.updatedUtcProperty, "datetime").label(updatedUtcLabel).editable(false)
                    .cssClasses(hiddenFieldClasses)
            },
            rule: {
                sourcePath: nga.field(models.rule.sourcePathProperty).label("Source Path").validation({
                    required: true,
                    pattern: /^\/[^?#]*$/,
                    maxlength: 255
                }).attributes({ placeholder: 'Enter path, starting with "/". Query string/hash are not allowed.' }),
                destinationUrl: nga.field(models.rule.destinationUrlProperty).label("Destination Url")
                    .validation({
                        required: true,
                        pattern: urlValidatorRegExp,
                        maxlength: 255
                    }).attributes({ placeholder: "http://someurl.com" }),
                createdUtc: nga.field(models.rule.createdUtcProperty, "datetime").label(createdUtcLabel)
                    .editable(false).cssClasses(hiddenFieldClasses),
                updatedUtc: nga.field(models.rule.updatedUtcProperty, "datetime").label(updatedUtcLabel).editable(false)
                    .cssClasses(hiddenFieldClasses)
            },
            binding: {
                name: nga.field(models.binding.nameProperty).label(bindingNameLabel).validation({
                    required: true,
                    validator: function(value) {
                        var port;
                        if (!value) return false;
                        const winName = /^[a-z][a-z0-9]*(?:\:(\d+))?$/gi.exec(value);

                        if (winName) {
                            port = winName[1];
                        } else {
                            const netName =
                                /^(?!:\/\/)(?:[a-zA-Z0-9-]+\.){0,5}[a-zA-Z0-9-][a-zA-Z0-9-]+\.[a-zA-Z]{2,64}?(?:\:(\d+))?$/gi
                                    .exec(value);

                            if (netName) {
                                port = netName[1];
                            } else {
                                throw new Error(
                                    "Invalid binding.  Binding should be in form '{hostname}' or '{hostname}:{portNumber}'");
                            }
                        }
                        if (port) {
                            const portErrorMessage = "Binding port number must be between 0 and 65535.";
                            if (port.length > 5) {
                                throw new Error(portErrorMessage);
                            } else {
                                const portAsNumber = parseInt(port);
                                if (portAsNumber < 0 || portAsNumber > 65535) {
                                    throw new Error(portErrorMessage);
                                }
                            }
                        }
                    }

                }).attributes({ placeholder: "myhostname.com" }).isDetailLink(true),
                createdUtc: nga.field(models.binding.createdUtcProperty, "datetime").label(createdUtcLabel)
                    .editable(false).cssClasses(hiddenFieldClasses),
                updatedUtc: nga.field(models.binding.updatedUtcProperty, "datetime").label(updatedUtcLabel)
                    .editable(false).cssClasses(hiddenFieldClasses)

            }
        };

        function makeSiteIdReferenceField(options) {
            options = options || {};
            return nga.field(models.site.siteIdProperty, "reference").isDetailLink(false)
                .label(options.label || siteGroupName).pinned(options.pinned || true)
                .targetEntity(entities.site)
                .targetField(fields.site.name)
                .sortField(models.site.nameProperty)
                .sortDir("ASC")
                .validation({ required: true })
                .remoteComplete(true,
                    {
                        //refreshDelay: 200,
                        searchQuery: search => ({ q: search })
                    });
        }

        function configureBinding() {
            const siteIdReferenceField = makeSiteIdReferenceField();
            const siteIdEditField = makeSiteIdReferenceField();

            const createFields = [
                siteIdEditField, fields.binding.name
            ];
            const editFields = [];
            angular.copy(createFields, editFields);
            editFields.push(fields.binding.createdUtc);
            editFields.push(fields.binding.updatedUtc);

            const showAndDeletionFields = [
                siteIdReferenceField, fields.binding.name, fields.binding.createdUtc,
                fields.binding.updatedUtc
            ];
            entities.binding.listView().fields(showAndDeletionFields).listActions(["edit", "delete"])
                .title(bindingItemName + "s").actions(["create"]).perPage(15).batchActions([]).filters(
                    [makeSiteIdReferenceField({ label: "Filter by Site", pinned: true })]);

            entities.binding.deletionView().fields(showAndDeletionFields)
                .title(`Delete ${bindingItemName} "{{entry.values.${models.binding.nameProperty}}}"?`)
                .description("This will permanently delete the binding.");
            entities.binding.creationView().fields(createFields).title(`Create New ${bindingItemName}`);
            entities.binding.editionView().fields(editFields).title(`Edit ${bindingItemName}`);
            return entities.binding;
        }

        function configureRule() {
            const ruleDescription =
                "For Source Path, enter the absolute path (starting with '/') you'd like to match.  Omit the query string or hash following the path.<br/><br/>For the destination URL, enter any valid URL that starts with 'http' or 'https'.";
            const createFields = [makeSiteIdReferenceField(), fields.rule.sourcePath, fields.rule.destinationUrl];
            const editFields = [];
            angular.copy(createFields, editFields);
            editFields.push(fields.rule.createdUtc);
            editFields.push(fields.rule.updatedUtc);


            entities.rule.listView().fields(editFields).actions(["create"])
                .filters([
                    makeSiteIdReferenceField({label:'Filter by Site'}),
                    nga.field("q", "template")
                    .label("Search")
                    .pinned(true)
                        .template(
                            '<div class="input-group"><input type="text" ng-model="value" placeholder="Search Text" class="form-control"></input><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span></div>')
                ])
                .listActions(["edit", "delete"]).title(ruleItemName + "s")
                .perPage(100).batchActions([]);

            entities.rule.deletionView().fields(editFields).title(`Delete ${ruleItemName}`);
            entities.rule.creationView().fields(createFields).title(`Create New ${ruleItemName}`)
                .description(ruleDescription);
            entities.rule.editionView().fields(editFields).title(`Edit ${ruleItemName}`).description(ruleDescription);

        }

        function configureSite() {
            // set the fields of the user entity list view
            const ruleField = nga.field("rules", "referenced_list").label(rulePluralLabel).perPage(999999)
                .targetEntity(entities.rule)
                .targetReferenceField(models.site.siteIdProperty)
                .targetFields([fields.rule.sourcePath, fields.rule.destinationUrl])
                .sortField(models.rule.sourcePathProperty).sortDir("ASC")
                .listActions(["edit", "delete"]);
            const bindingField = nga.field("bindings", "referenced_list").label(bindingPluralLabel).perPage(999999)
                .targetEntity(entities.binding)
                .targetReferenceField(models.site.siteIdProperty)
                .targetFields([fields.binding.name]).sortField(models.binding.nameProperty).sortDir("ASC")
                .listActions(["edit", "delete"]);
            const importRulesField = nga.field("custom_action")
                .label("")
                .template(
                    '<import-rules site="entry"></import-rules><export-rules site="entry" style="margin-left:5px"></export-rules>');


            entities.site.listView().fields([
                fields.site.name, importRulesField, fields.site.createdUtc, fields.site.updatedUtc
            ]).listActions(["edit", "delete"]).title(siteItemName + "s").actions(["create"])
                .perPage(30)
                .sortField(models.site.nameProperty)
                .sortDir("ASC")
                .batchActions([]);

            entities.site.creationView().fields([
                fields.site.name
            ]).title(`Create New ${siteItemName}`);

            entities.site.editionView().fields([
                fields.site.name,
                bindingField,
                ruleField,
                importRulesField, fields.site.createdUtc, fields.site.updatedUtc
            ]).title(`Edit ${siteItemName}`);

            entities.site.deletionView().fields([
                fields.site.name, fields.site.createdUtc, fields.site.updatedUtc
            ]).title(`Delete ${siteItemName}`);


            return entities.site;
        }


        //// create an admin application
        const admin = nga.application("Administration").baseApiUrl("/api/"); // main API endpoint

        entities.site = nga.entity("site").identifier(nga.field(models.site.siteIdProperty)).label(sitePluralLabel);
        entities.binding = nga.entity("binding").identifier(nga.field("BindingId")).label(bindingPluralLabel);
        entities.rule = nga.entity("rule").identifier(nga.field("RuleId")).label(rulePluralLabel);

        configureSite();
        configureRule();
        configureBinding();
        admin.dashboard(nga.dashboard()
            .addCollection(nga.collection(entities.site)
                .name("sites")
                .title("Sites")
                .perPage(30) // limit the panel to the 5 latest posts
                .fields([
                    fields.site.name

                    /*,
                    nga.field('title').isDetailLink(true).map(truncate),
                    nga.field('views', 'number')*/
                ])
                .sortField(models.site.nameProperty)
                .sortDir("ASC")
                .order(1).listActions(["edit", "delete"])
            )
        );


        // add the user entity to the admin application
        admin.addEntity(entities.site);
        admin.addEntity(entities.binding);
        admin.addEntity(entities.rule);
        //// more configuration here later
        //// ...
        //// attach the admin application to the DOM and run it
        nga.configure(admin);


    }
]);