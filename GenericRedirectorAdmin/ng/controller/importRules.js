﻿myApp.controller("importRulesController",
    [
        "$scope", "$http", "$stateParams", "notification", "fileUpload",
        function($scope,
            $http,
            $stateParams,
            notification,
            fileUpload
        ) {
            $scope.done = false;
            $scope.busy = false;
            $http({
                method: "GET",
                url: `/api/site/${$stateParams.SiteId}`
            }).then(function(response) {
                    $scope.siteInfo = response.data;
                },
                function(err) {
                    alert("b");
                });

            $scope.uploadFile = function() {
                $scope.busy = true;
                const file = $scope.myFile;
                console.log("file is ");
                console.dir(file);
                fileUpload.uploadFileToUrl(file, "/importexport/doimport", { siteId: $stateParams.SiteId }).then(function(data) {
                    $scope.done = true;
                    $scope.postResponse = data;
                    $scope.busy = false;
                }, function(err) {
                    alert("An error occurred");
                    $scope.busy = false;
                });
            };
        }
    ]
);