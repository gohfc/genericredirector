﻿myApp.service("fileUpload",
    [
        "$http", "$q", function($http, $q) {
            this.uploadFileToUrl = function(file, uploadUrl, data) {
                var deferred = $q.defer();
                const fd = new FormData();
                fd.append("file", file);
                for (let s in data) {
                    if (data.hasOwnProperty(s)) {
                        fd.append(s, data[s]);
                    }
                }

                $http.post(uploadUrl,
                        fd,
                        {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        })
                    .then(function(response) {
                            deferred.resolve(response.data);
                        },
                        function(err) {
                            deferred.reject(err);
                        });
                return deferred.promise;
            };
        }
    ]);