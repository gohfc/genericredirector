﻿using System;
using System.IO;
using System.Net;
using System.Security.Policy;
using ExcelDataReader;

namespace GenericRedirectorTester
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var filePath = "Budget Blinds Blog Redirects.xlsx";
            using (var stream = File.OpenRead(filePath))
            {
                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    int? oldColumn = null;
                    int? newColumn = null;


                    var first = true;
                    // Choose one of either 1 or 2:

                    // 1. Use the reader methods
                    do
                    {
                        while (reader.Read())
                            if (first)
                            {
                                first = false;
                                for (var x = 0; x < reader.FieldCount; x++)
                                    if (reader[x] != null)
                                    {
                                        var z1 = reader[x].ToString();
                                        switch (z1)
                                        {
                                            case "New URL":
                                                newColumn = x;
                                                break;
                                            case "Old":
                                                oldColumn = x;
                                                break;
                                        }
                                    }
                            }
                            else if (oldColumn.HasValue && newColumn.HasValue)
                            {
                                var oldUrl = reader[oldColumn.Value].ToString();
                                var newUrl = reader[newColumn.Value].ToString();
                                Uri tmp;
                                if (Uri.TryCreate(oldUrl, UriKind.RelativeOrAbsolute, out tmp))
                                {
                                    oldUrl = oldUrl.Replace("https://blog.budgetblinds.com/",
                                        "http://localhost:52096/");
                                    var z = WebRequest.CreateHttp(oldUrl);
                                    z.AllowAutoRedirect = false;
                                    Console.WriteLine("Trying {0}", oldUrl);
                                    using (var m = z.GetResponse())
                                    {
                                        var aa = m.Headers["Location"];

                                        if (aa != newUrl)
                                        {
                                            Console.WriteLine("New location {0} doesn't match {1}", aa, newUrl);
                                        }
                                    }
                                }
                            }

                    } while (reader.NextResult());


                    // The result of each spreadsheet is in result.Tables
                }
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}